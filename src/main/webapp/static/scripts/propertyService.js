    var houseList = null;
    var googleMapLoaded = false;
	function getHouses(dataArray)
	{
		
		 $.ajax({
			 url: "GetHouses",
			 method: "post",
			 data: dataArray,
		 	 dataType : "json",
		 	 global : false,
			 success: function(response){
				if(googleMapLoaded)
			    {
					setMapOnAll(null); 
			    }
			    houseList = response.data;
				var size = houseList.length;
				$("#dataView").html("");
				for(var i=0;i<size;i++)
				{
					var html ='<tr>'
						+'<td>'+(i+1)+'</td>'
				        +'<td><img src="static/images/northStarMls/'+houseList[i].images[0].name+'" alt="Los Angeles" height="100" width="200"></td>'
				        +'<td><b>MLS #: '+houseList[i].MLNumber+'</b></td>'
				        +'<td><b>Price: $'+houseList[i].LISTPRICE+'</b></td>'
				        +'<td><button class="btn btn-primary" onclick="showDetailArea('+i+')">More Info</button></td>'
				        +'</tr>';
				        $("#dataView").append(html);
				        if(googleMapLoaded)
				        {
				        	var latLng = {lat: parseFloat(houseList[i].LATITUDE), lng: parseFloat(houseList[i].LONGITUDE)};
				        	 var marker = new google.maps.Marker({
				   	          position: latLng,
				   	          map: map,
				   	          title: 'Price: $' + houseList[i].LISTPRICE
				   	        });
					          
					        markers.push(marker);
				        }
				       

				}
				$("#paginationArea").html('<p><b>showing '+(response.total > dataArray.pageSize ? dataArray.pageSize : response.total) + ' of ' + response.total + '</b></p>');
		 	 },
		 	 error: function(result){
		 	 }
		})
	}
	
	function getSearchCriteria()
	{
		var dataArray = {
				"bedrooms" : $("#bedrooms").val(),
				"bathrooms" : $("#bathrooms").val(),
				"minPrice" : $("#minPrice").val(),
				"maxPrice" : $("#maxPrice").val(),
				"pageNo" : 1,
				"pageSize" : 50
		}
		getHouses(dataArray);
	}
	function getPolygonCoveredHouses(locationLat, locationLng)
	{
		var dataArray = {
				"bedrooms" : $("#bedrooms").val(),
				"bathrooms" : $("#bathrooms").val(),
				"minPrice" : $("#minPrice").val(),
				"maxPrice" : $("#maxPrice").val(),
				"locationLat" : locationLat,
				"locationLng" : locationLng,
				"polygonSearch" : true,
				"pageNo" : 1,
				"pageSize" : 50
			}
		getHouses(dataArray);
	}
	
	function getRectangleCoveredHouses(northEastLat,northEastLng, southWestLat,southWestLng)
	{
		var dataArray = {
				"bedrooms" : $("#bedrooms").val(),
				"bathrooms" : $("#bathrooms").val(),
				"minPrice" : $("#minPrice").val(),
				"maxPrice" : $("#maxPrice").val(),
				"northEastLat" : northEastLat,
				"northEastLng" :northEastLng,
				"southWestLat" :southWestLat,
				"southWestLng" :southWestLng,
				"rantangleSearch" : true,
				"pageNo" : 1,
				"pageSize" : 50
			}
		getHouses(dataArray);
	}
	
	function getCircleCoveredHouses(circleCenterLat, circleCenterLng,circleRadius) {
		var dataArray = {
				"bedrooms" : $("#bedrooms").val(),
				"bathrooms" : $("#bathrooms").val(),
				"minPrice" : $("#minPrice").val(),
				"maxPrice" : $("#maxPrice").val(),
				"circleCenterLat" : circleCenterLat,
				"circleCenterLng" :circleCenterLng,
				"circleRadius" :circleRadius,
				"circleSearch" : true,
				"pageNo" : 1,
				"pageSize" : 50
			}
		getHouses(dataArray);
	}
	function showDetailArea(index)
	{
		var house = houseList[index];
				
		var html = '<h4><b>MLS #: '+house.MLNumber+'</b></h4>'
		           +'<h4><b>Price: $'+house.LISTPRICE+'</b></h4>'
		           +'<h4><b>'+house.StreetNumber + ' '  + house.STREETNAME + ', ' +house.City + ', '+house.StateOrProvince +' ' +house.PostalCode +'</b></h4>';
		
		$("#property_basic_info").html(html);
		
		
		html  =  '<h2>Property Features</h2>' 
		         +'<hr>'
		         +'<div class="col-md-6">'
			     +'<p><b> Bedrooms:</b> '+house.Bedrooms+'</p>'
			     +'<p><b> Bathrooms:</b> '+house.BATHSTOTAL+'</p>'
			     +'<p><b>Built in Year:</b> '+house.YEARBUILT+'</p>'
			     +'<p><b>Appliances:</b> '+house.APPLIANCES+'</p>'
			     +'<p><b>Foundation Size:</b> '+house.FOUNDATIONSIZE+'</p>'
			     +'<p><b>Style:</b> '+house.STYLE+'</p>'
			     +'<p><b>County:</b> '+house.COUNTY+'</p>'
			     +'</div>'
			     +'<div class="col-md-6">'
			     +'<p><b> Number of Fireplaces:</b> '+house.Fireplaces+'</p>'
			     +'<p><b> Amenities - Unit:</b> '+house.AMENITIESUNIT+'</p>'
			     +'<p><b>Acress:</b> '+house.ACRES+'</p>'
			     +'<p><b>School District: </b> '+house.SchoolDistrictNumber+'</p>'
			     +'</div>';
		$("#property_more_info").html(html);
         
         var size = 0;
         if(house.images != undefined)
        	 size = house.images.length;
         var indicatiors = "";
         var inner = "";
         for(var i=1;i<size;i++)
         {
        	 if(i==1)
        	 {
        		 indicatiors +='<li data-target="#myCarousel" data-slide-to="'+i+'" class="active"></li>';
        		 inner+='<div class="item active"><img src="static/images/northStarMls/'+house.images[i].name+'" alt="Los Angeles"  width="865" height="500">'
        		 	   +'<div class="text text-center">'+(house.images[i].description == undefined || house.images[i].description == null?"":house.images[i].description)+'</div>'
        		 	   +'</div>';
        	 }
        	 else
        	 {
        		 indicatiors +='<li data-target="#myCarousel" data-slide-to="'+i+'"></li>';
        		 inner+='<div class="item"><img src="static/images/northStarMls/'+house.images[i].name+'" alt="Los Angeles"  width="865" height="500">'
        		      +'<div class="text text-center">'+(house.images[i].description == undefined || house.images[i].description == null?"":house.images[i].description)+'</div>'
        		      +'</div>';
        	 }
         }
         
         $("#carouselIndicators").html(indicatiors);
         $("#carouselInner").html(inner);
         
         $("#dataArea").hide();
 		 $("#detailArea").show();
 		 
 		loadPropertyMap(house.LATITUDE, house.LONGITUDE);
		
	}
	
	function backToDataArea()
	{
		$("#detailArea").hide();
		$("#dataArea").show();
		
	}