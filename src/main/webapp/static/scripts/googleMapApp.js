   var map;
   var markers = [];

    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
    var selectedShape = null;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 44.987747, lng: -93.310051},
        zoom: 8
      });
        
       googleMapLoaded = true;
       
      var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [/*'marker'*/, 'circle', 'polygon', /*'polyline'*/, 'rectangle']
        },
        markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
        circleOptions: {
         // fillColor: '#ffff00',
          //fillOpacity: 1,
          //strokeWeight: 5,
          clickable: false,
          editable: true,
          zIndex: 1
        },
        rectangleOptions : {
            clickable: true,
            editable: true,
            draggable : true

        }
      });
      
      drawingManager.setMap(map);
      
      google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
    	  if(selectedShape != null)
    		  selectedShape.setMap(null);
    	  selectedShape = e.overlay;
    	  
	    if (e.type == 'rectangle') {
	    
	      var northEast = e.overlay.getBounds().getNorthEast();
	      var southWest = e.overlay.getBounds().getSouthWest();
	      console.log("northEast: "+ northEast);
	      console.log("southWest: "+ southWest);
	      getRectangleCoveredHouses(northEast.lat(),northEast.lng(),southWest.lat(),southWest.lng())
	    }
	    
	    else if (e.type == 'polygon') {
	    	  var locationLat =  "";
	    	  var locationLng = "";
	    	  var myMvcArray = e.overlay.getPath();

	    	  myMvcArray.getArray().forEach(function(value, index, array_x) {
	    		    console.log(" index: " + index + "    lat: " + value.lat() +"    lng: " + value.lng());
	    		    locationLng+=value.lng()+"&";
	    		    locationLat+=value.lat()+"&";
	    		    
	    		})
	    		getPolygonCoveredHouses(locationLat, locationLng);
	    }
	    
	    else if (e.type == 'circle') {
	    	  var circleCenterLat =  e.overlay.getCenter().lat();
	    	  var circleCenterLng = e.overlay.getCenter().lng();
	    	  var circleRadius = e.overlay.getRadius();
              console.log("lat: " + circleCenterLat + ", lng: " +circleCenterLng + ": radius: " + circleRadius );
	    	  getCircleCoveredHouses(circleCenterLat, circleCenterLng,circleRadius);
	    }
      
	  });
    }
    
    function loadPropertyMap(lattitude, longitude) {
		 var myLatLng = {lat:lattitude, lng:longitude};

	        var map = new google.maps.Map(document.getElementById('property_map'), {
	          zoom: 4,
	          center: myLatLng
	        });

	        var marker = new google.maps.Marker({
	          position: myLatLng,
	          map: map,
	          title: 'Hello World!'
	        });
     }
    
    function setMapOnAll(map)
    {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
   }
