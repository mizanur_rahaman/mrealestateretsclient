<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home Sale</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- library js block start -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbe7k_UptlRJv7oHJ5nCce26QN6iJ4f2s&libraries=drawing"
	async defer></script>
<!-- library js block end -->

<!-- app js block start -->
<script src="static/scripts/propertyService.js"></script>
<script src="static/scripts/googleMapApp.js"></script>
<!-- library js block end -->
<style>

/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
</style>
<script type="text/javascript">
  
	
	$( document ).ready(function() {
		initMap();
		getSearchCriteria();
	});
	

</script>

</head>

<body>


	<div class="panel panel-default">
		<div class="panel-heading">
			<h2 class="text-center">Real Estate</h2>
		</div>
		<div class="row" id="dataArea">
			<div class="col-md-5 panel-body panel-primary" id="map"
				style="height: 500px;"></div>
			<div class="col-md-7">
				<div class="panel-body panel-primary">
					<div class="panel-body panel-primary" id="authenticateForm">
						<form class="form-inline" action="/action_page.php">
							<div class="form-group">
								<input type="text" class="form-control" id="minPrice"
									name="minPrice" placeholder="Min Price">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="maxPrice"
									name="maxPrice" placeholder="Max Price">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id=bedrooms
									name="bedrooms" placeholder="Bedrooms">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="bathrooms"
									name="bathrooms" placeholder="Bathrooms">
							</div>
							<button type="button" class="btn btn-primary"
								onclick="getSearchCriteria()">Search</button>
						</form>
					</div>
					<div class="panel-body panel-primary">
						<div style="max-height: 700px; overflow-y: auto">
							<table class="table">
								<tbody id="dataView">
								</tbody>
							</table>
						</div>
						<div id="paginationArea"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body panel-primary" style="margin-left: 20px;display:none" id="detailArea" >
			<button type="button" class="btn btn-primary"
				onclick="backToDataArea()">Back</button>
			<br>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div id="myCarousel" style="width: 95%;" class="carousel slide"
						data-ride="carousel">
						<ol class="carousel-indicators" id="carouselIndicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						</ol>
						<div class="carousel-inner" id="carouselInner"></div>
						<a class="left carousel-control" href="#myCarousel"
							data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span> <span
							class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel"
							data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"></span> <span
							class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<div class="col-md-6" >
				    <div id="property_basic_info">
				    </div>
					<br>
					<div id="property_map" style="width: 400px; height: 400px; background: yellow"></div>
				</div>
			</div>
			<div class="row" id="property_more_info">
			</div>
		</div>
	</div>
</body>
</html>