<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home Sale</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function getData()
{
	var dataArray = {
		"class" : $("#className").val(),
		"table" : $("#tableName").val(),
		"offset" : $("#startRow").val(),
		"limit" : $("#limit").val()
	
	}
	 $("#messageArea").html("Data Retieving......");
	 $.ajax({
		 url: "RetrieveData",
		 method: "get",
		 data: dataArray,
	 	 dataType : "json",
	 	 global : false,
		 success: function(response){
			 $("#messageArea").html("Completed Data Retrieving");
	 	 },
	 	 error: function(result){
	 		$("#messageArea").html("Completed Data Retrieving");
	 	 }
	});
}
</script>
</head>

<body>
    <div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>Real Estate</h2>
			</div>
			
			<div class="panel-body panel-primary" id="dataArea" >
				<div class="panel-body panel-primary" id="authenticateForm">
					<form class="form-inline" action="/action_page.php">
					    <div id="messageArea"></div>
						<div class="form-group">
							<input type="text" class="form-control" id="className"
								name="className" placeholder="Class">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="tableName"
								name="tableName" placeholder="Table Name">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id=startRow
								name="startRow" placeholder="Start Row">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="limit"
								name="limit" placeholder="Limit">
						</div>
						<button type="button" class="btn btn-primary"
							onclick="getData()">Retrieve Data</button>
					</form>
				</div>
			</div>
		</div>
		
		</div>	
	</body>
</html>