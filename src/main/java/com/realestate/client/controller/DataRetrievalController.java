package com.realestate.client.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.client.service.RealEstateDataService;

@RestController
public class DataRetrievalController {
	@Autowired RealEstateDataService realEstateService;

	@RequestMapping("/RetriveDataView")
	public ModelAndView retriveDataPage()
	{
		//realEstateDataService.getMetaData();
		//realEstateDataService.getData("RES","Single-Family");
		//realEstateDataService.storeImages();
		return new ModelAndView("home/RetreiveData");
	}
	
	@RequestMapping("/RetrieveData")
	public String retriveData(HttpServletRequest request, HttpServletResponse response)
	{
		realEstateService.getData(request.getParameter("class"), request.getParameter("table"),request.getParameter("offset"),request.getParameter("limit"));
		return "success";
	}
}
