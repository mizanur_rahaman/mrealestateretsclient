package com.realestate.client.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.realestate.client.dto.ResultDto;
import com.realestate.client.form.PropertySearchForm;
import com.realestate.client.service.PropertyService;

@RestController
public class PropertyController {

   @Autowired PropertyService propertyService;
   

   @RequestMapping(value="/GetHouses",method= {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultDto getProperty(HttpServletRequest request, HttpServletResponse response,PropertySearchForm propertySearchForm) 
	{
		System.out.println("cred "+ propertySearchForm);
		return  propertyService.getHouses(propertySearchForm);
	}
}
