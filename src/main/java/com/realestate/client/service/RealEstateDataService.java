package com.realestate.client.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.realtors.rets.client.GetObjectRequest;
import org.realtors.rets.client.RetsException;
import org.realtors.rets.client.RetsSession;
import org.realtors.rets.client.SearchRequest;
import org.realtors.rets.client.SearchResultImpl;
import org.realtors.rets.client.SingleObjectResponse;
import org.realtors.rets.common.metadata.types.MClass;
import org.realtors.rets.common.metadata.types.MResource;
import org.realtors.rets.common.metadata.types.MSystem;
import org.realtors.rets.common.metadata.types.MTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoDatabase;
import com.realestate.client.dao.PropertyDao;
import com.realestate.client.dao.RealEstateDataDao;
import com.realestate.client.dto.ResultDto;
import com.realestate.client.manager.connection.MongoDbConnectionManager;
import com.realestate.client.manager.connection.RetsConnectionManager;

@Service
public class RealEstateDataService {
	
	@Autowired RetsConnectionManager retsConnectionManager;
	@Autowired RealEstateDataDao realEstateDao;
	@Autowired MongoDbConnectionManager mongoDbConnectionManager;
	@Autowired PropertyDao propertyDao;
	
	public void getMetaData()
	{
		RetsSession retsSession = retsConnectionManager.getRetsConnection("GET");
		try {
			MSystem system = retsSession.getMetadata().getSystem();
			System.out.println(
					"SYSTEM: " + system.getSystemID() + 
					" - " + system.getSystemDescription());

			for(MResource resource: system.getMResources()) {

				System.out.println(
						"    RESOURCE: " + resource.getResourceID());
                if(resource.getResourceID().equalsIgnoreCase("Property"))
                {
					for(MClass classification: resource.getMClasses()) {
						System.out.println(
								"        CLASS: " + classification.getClassName() +
								" - " + classification.getDescription());
						realEstateDao.insertNewProperty(classification.getDescription(), classification.getClassName());
						//if(classification.getClassName().equals("RES"))
						//{
							 for(MTable table : classification.getMTables()){
								 System.out.print(table.getDBName()+", ");
								 //System.out.println(table.getKnownAttributes());
							 }
						//}
					}
                }
			}
			
		}
		catch (RetsException e) {
			e.printStackTrace();
		} 	
		finally {
			if(retsSession != null) { 
				try {
					retsSession.logout(); 
				} 
				catch(RetsException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void getData(String className,String tableName, String offset, String limit)
	{
		RetsSession retsSession = retsConnectionManager.getRetsConnection("GET");
		
		HashMap<String,String> dataTypesMap = this.getFieldDataType(retsSession,className);
		
		String sQuery = "(ACRES=0-20)";
		String sResource = "Property";
		String sClass = className;
		
		//Create a SearchRequest
		SearchRequest request = new SearchRequest(sResource, sClass, sQuery);

		//Select only available fields
		String select ="*";
		request.setSelect(select);
		request.setOffset(Integer.parseInt(offset));
		request.setLimit(Integer.parseInt(limit));

		//Set request to retrive count if desired
		request.setCountFirst();

		SearchResultImpl response;
		try {
			//Execute the search
			response= (SearchResultImpl) retsSession.search(request);
			//Print out count and columns
			int count = response.getCount();
			
			System.out.println("COLUMNS: " + StringUtils.join(response.getColumns(), "\t"));
            String columns[] = response.getColumns();
			//Iterate over, print records
			for (int row = 0; row < response.getRowCount(); row++){
				
				String rows[] = response.getRow(row);
				String UID = null;
				Document document = new Document();
				for(int i=0;i<columns.length;i++)
				{
					if(dataTypesMap.get(columns[i]).equalsIgnoreCase("Int") )
						document.append(columns[i], rows[i] == null || rows[i].trim().length() == 0 ? 0 : Integer.parseInt(rows[i]));
					else if(dataTypesMap.get(columns[i]).equalsIgnoreCase("Long") )
						document.append(columns[i], rows[i] == null || rows[i].trim().length() == 0 ? 0 : Long.parseLong(rows[i]));
					else if(dataTypesMap.get(columns[i]).equalsIgnoreCase("Decimal") )
						document.append(columns[i], rows[i] == null || rows[i].trim().length() == 0 ? 0 : Double.parseDouble(rows[i]));
					else if(dataTypesMap.get(columns[i]).equalsIgnoreCase("Boolean") )
						document.append(columns[i], rows[i] == null || rows[i].trim().length() == 0 ? 0 : Boolean.parseBoolean(rows[i]));
					else if(dataTypesMap.get(columns[i]).equalsIgnoreCase("DateTime") )
						document.append(columns[i], rows[i]);
					else if(dataTypesMap.get(columns[i]).equalsIgnoreCase("Date"))
						document.append(columns[i], rows[i]);
					else
						document.append(columns[i], rows[i]);
					
					if(columns[i].equalsIgnoreCase("UID"))
						UID = rows[i];
				}
				if(UID != null)
				{
					List<String> idsList = new ArrayList<String>();
					idsList.add(UID);
					document.append("images",this.getImage(idsList,tableName ));
				}
				
				// add location
				ArrayList<Double> location = new ArrayList<Double>();
				location.add(document.getDouble("LONGITUDE"));
				location.add(document.getDouble("LATITUDE"));
				
				document.append("location",location );
				
				System.out.println("ROW "+ row +": UID - " +UID);
				realEstateDao.insertNewHouse(document,tableName);
			}
		} catch (RetsException e) {
			e.printStackTrace();
		} 
		finally {
			if(retsSession != null) { 
				try {
					retsSession.logout(); 
				} 
				catch(RetsException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Document> getImage(List<String> idsList,String imageFolderName)
	{
		RetsSession retsSession = retsConnectionManager.getRetsConnection("GET");
		String sResource = "Property";
		String objType   = "Photo";
		String seqNum 	= "*"; // * denotes get all pictures associated with id (from Rets Spec)
		List<Document> imageList = new ArrayList<Document>();
		try {
			//Create a GetObjectRequeset
			GetObjectRequest req = new GetObjectRequest(sResource, objType);

			//Add the list of ids to request on (ids can be determined from records)
			Iterator<String> idsIter = idsList.iterator();
			while(idsIter.hasNext()) {
				req.addObject(idsIter.next(), seqNum);
			}

			//Execute the retrieval of objects 
			Iterator<SingleObjectResponse> singleObjectResponseIter = retsSession.getObject(req).iterator();
            
			
			//Iterate over each Object 
			while (singleObjectResponseIter.hasNext()) {
				SingleObjectResponse sor = (SingleObjectResponse)singleObjectResponseIter.next();

				//Retrieve in info and print
				String type =			sor.getType();
				String contentID = 		sor.getContentID();
				String objectID = 		sor.getObjectID();
				String description = 	sor.getDescription();
				String location = 		sor.getLocation();
				InputStream is = 		sor.getInputStream();

				/*System.out.print("type:" + type);
				System.out.print(" ,contentID:" + contentID);
				System.out.print(" ,objectID:" + objectID);
				System.out.println(" ,description:" + description);
				System.out.println("location:" + location);*/
				
			

				//Download object
				try {
					String dest			= "E:\\JAVA\\RealEstateRetsClient\\src\\main\\webapp\\static\\images\\northStarMls\\"+imageFolderName+"\\";
					int size = is.available();
					String imageName = contentID +"-" + objectID + "." + (type != null?type.split("/")[1] : "jpeg");
					Document document = new Document();
					document.append("name", imageFolderName +"/"+imageName);
					document.append("description", description);
					imageList.add(document);
					String filename = dest + imageName ;
					OutputStream out = new FileOutputStream(new File(filename)); 
					int read = 0;
					byte[] bytes = new byte[1024];

					while ((read = is.read(bytes)) != -1) {

						out.write(bytes, 0, read);
					}

					is.close();
					out.flush();
					out.close();

					System.out.println("New file with size " + size + " created: " + filename);
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}

			}

		} catch (RetsException e) {
			e.printStackTrace();
		}
		finally {
			if(retsSession != null) {
				try {
					retsSession.logout();
				}
				catch (RetsException e) {
					e.printStackTrace();
				}
			}
		}
		return imageList;
	}
	
	public void storeImages()
	{
		MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		
		ResultDto resultDto = propertyDao.getSingleFamilyHouses(database, new Document(),0,100);
	    ArrayList<Document> houseList = resultDto.getData(); 
		for(int i=0;i<houseList.size();i++)
		{
			if(i>73){
				Document doc = houseList.get(i);
				List<String> ids= new ArrayList<String>();
				ids.add(doc.get("UID")+"");
				List<Document> imageList = this.getImage(ids,"Single-Family");
				
				
				Document newDocument = new Document();
				newDocument.append("$set", new Document().append("images",imageList));
		
				Document searchQuery = new Document().append("UID", doc.get("UID"));
				realEstateDao.addSingleFamilyHousesImage( searchQuery, newDocument);
			}
			
		}
	}
	
	public HashMap<String,String> getFieldDataType(RetsSession retsSession, String className)
	{
		HashMap<String,String> dataTypesMap = new HashMap<String,String>();
		try {
			MSystem system = retsSession.getMetadata().getSystem();
			System.out.println(
					"SYSTEM: " + system.getSystemID() + 
					" - " + system.getSystemDescription());

			for(MResource resource: system.getMResources()) {

				System.out.println(
						"    RESOURCE: " + resource.getResourceID());
                if(resource.getResourceID().equalsIgnoreCase("Property"))
                {
					for(MClass classification: resource.getMClasses()) {
						System.out.println(
								"        CLASS: " + classification.getClassName() +
								" - " + classification.getDescription());
						//realEstateDao.insertNewProperty(classification.getDescription(), classification.getClassName());
						if(classification.getClassName().equals(className))
						{
							 for(MTable table : classification.getMTables()){
								 System.out.print(table.getId()+", ");
								 dataTypesMap.put(table.getId(), table.getDataType());
							 }
						}
					}
                }
			}
			
		}
		catch (RetsException e) {
			e.printStackTrace();
		} 	
		finally {
			if(retsSession != null) { 
				try {
					retsSession.logout(); 
				} 
				catch(RetsException e) {
					e.printStackTrace();
				}
			}
		}
		return dataTypesMap;
	}
}
