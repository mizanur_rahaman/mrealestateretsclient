package com.realestate.client.service;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoDatabase;
import com.realestate.client.dao.PropertyDao;
import com.realestate.client.dto.ResultDto;
import com.realestate.client.form.PropertySearchForm;
import com.realestate.client.manager.connection.MongoDbConnectionManager;

@Service
public class PropertyService {
	@Autowired MongoDbConnectionManager mongoDbConnectionManager;
	@Autowired PropertyDao propertyDao;
	
	public ResultDto getHouses(PropertySearchForm propertySearchForm)
	
	{
		 MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		 Document criteria = new Document();
		 if(propertySearchForm.getBedrooms() != null && propertySearchForm.getBedrooms().trim().length() > 0)
		 {
			 criteria.put("Bedrooms", Integer.parseInt(propertySearchForm.getBedrooms()));
		 }
		 if(propertySearchForm.getBathrooms() != null && propertySearchForm.getBathrooms().trim().length() > 0)
		 {
			 criteria.put("BATHSTOTAL", Integer.parseInt(propertySearchForm.getBathrooms()));
		 }
		 if(propertySearchForm.getMinPrice() != null && propertySearchForm.getMinPrice().trim().length() > 0)
		 {
			 criteria.put("LISTPRICE", new Document("$gt", Double.parseDouble(propertySearchForm.getMinPrice())).append("$lt", Double.parseDouble(propertySearchForm.getMaxPrice())));
		 }
		 if(propertySearchForm.isRantangleSearch())
		 {
			 criteria.put("LATITUDE", new Document("$gt", propertySearchForm.getSouthWestLat()).append("$lt", propertySearchForm.getNorthEastLat()));
			 criteria.put("LONGITUDE", new Document("$gt", propertySearchForm.getSouthWestLng()).append("$lt", propertySearchForm.getNorthEastLng()));
		 }
		 
		 else if(propertySearchForm.isPolygonSearch())
		 {
			 System.out.println(propertySearchForm.getLocationLat() + " sf " + propertySearchForm.getLocationLng());
			 String locationLat[] = propertySearchForm.getLocationLat().split("&");
			 String locationLng[] = propertySearchForm.getLocationLng().split("&");
			 ArrayList location = new ArrayList();
			 for(int i=0;i<locationLat.length;i++)
			 {
				 ArrayList<Double> point = new ArrayList<Double>();
				 point.add(Double.parseDouble(locationLng[i]));
				 point.add(Double.parseDouble(locationLat[i]));
				 location.add(point);
			 }
			 criteria.put("location", new Document("$geoWithin",new Document("$polygon",location)));
		 }
		 
		 else if(propertySearchForm.isCircleSearch())
		 {
			 double radiusInRadiun = (propertySearchForm.getCircleRadius() /1000 ) / 6378.1; /* radius in meter, convert it kilometers, convert it radian */
			 ArrayList<Double> center = new ArrayList<Double>();
			 center.add(propertySearchForm.getCircleCenterLng());
			 center.add(propertySearchForm.getCircleCenterLat());
			 ArrayList location = new ArrayList();
			 location.add(center);
			 location.add(radiusInRadiun);
			 
			 criteria.put("location", new Document("$geoWithin",new Document("$centerSphere",location)));
		 }
		 int offset = (propertySearchForm.getPageNo() - 1 ) * propertySearchForm.getPageSize();
		 ResultDto resultDto = propertyDao.getSingleFamilyHouses(database,criteria,offset,propertySearchForm.getPageSize());
		 return resultDto;
	}
	
	public void updateHouses(ArrayList<Document> houseList)
	{
		MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		for(int i = 0;i < houseList.size();i++)
		{
			Document doc = houseList.get(i);

				Document newDocument = new Document();
				ArrayList<Double> location = new ArrayList<Double>();
				ArrayList<Document> imageList = (ArrayList<Document>) doc.get("images");
				ArrayList<Document> imageListNew = new ArrayList<Document>();
				for(int j=0;j<imageList.size();j++)
				{
					Document image = imageList.get(j);
					Document newImage = new Document();
					newImage.append("name", "Single-Family/"+image.getString("name")).append("description", image.getString("description"));
					imageListNew.add(newImage);
				}
				
				newDocument.append("$set", new Document().append("images",imageListNew));
				System.out.println("UID "+doc.get("UID"));
				Document searchQuery = new Document().append("UID", doc.getInteger("UID"));
				propertyDao.updateDocumentSingleFamilyHouse(database, searchQuery, newDocument);
			
		}
	}
	
	
}
