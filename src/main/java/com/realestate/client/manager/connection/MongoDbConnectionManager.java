package com.realestate.client.manager.connection;

import org.springframework.stereotype.Repository;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;

@Repository
public class MongoDbConnectionManager {
	public MongoDatabase getMongoDbConnection()
	{
		   MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
		   	      MongoCredential credential; 
	      credential = MongoCredential.createCredential("mongo", "mongodb", 
	         "password".toCharArray()); 
	      System.out.println("Connected to the database successfully");  
	      
	      // Accessing the database 
	      MongoDatabase database = mongo.getDatabase("north_star_mls_db");
	      return database;
	}
}
