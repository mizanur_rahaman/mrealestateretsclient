package com.realestate.client.manager.connection;

import org.realtors.rets.client.CommonsHttpClient;
import org.realtors.rets.client.RetsException;
import org.realtors.rets.client.RetsHttpClient;
import org.realtors.rets.client.RetsSession;
import org.realtors.rets.client.RetsVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RetsConnectionManager {

	@Autowired RetsConnection retsConnection;
	public RetsSession getRetsConnection(String method)
	{
		RetsHttpClient httpClient = new CommonsHttpClient();
		RetsVersion retsVersion = RetsVersion.RETS_1_7_2;
		System.out.println(retsConnection.getUrl() + " " + retsConnection.getUsername() + " " + retsConnection.getPassword());
		String loginUrl = retsConnection.getUrl();

		//Create a RetesSession with RetsHttpClient
		RetsSession session = new RetsSession(loginUrl, httpClient, retsVersion);    

		String username = retsConnection.getUsername();
		String password = retsConnection.getPassword();

		//Set method as GET or POST
		session.setMethod(method);
		try {
			//Login
			session.login(username, password);
		} catch (RetsException e) {
			e.printStackTrace();
		}
		return session;
	}
}
