package com.realestate.client.form;

import java.io.Serializable;

public class PropertySearchForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private String  bedrooms;
	private String  bathrooms;
	private String minPrice ;
	private String maxPrice ;
	private double northEastLat;
	private double northEastLng;
	private double southWestLat ;
	private double southWestLng ;
	private String locationLat;
	private String locationLng;
	private double circleCenterLat;
	private double circleCenterLng;
	private double circleRadius;
	private boolean rantangleSearch;
	private boolean polygonSearch;
	private boolean circleSearch;
	private int pageSize = 20;
	private int pageNo = 1;

	
	public void setNorthEastLat(double northEastLat) {
		this.northEastLat = northEastLat;
	}
	public double getNorthEastLng() {
		return northEastLng;
	}
	public void setNorthEastLng(double northEastLng) {
		this.northEastLng = northEastLng;
	}
	public double getSouthWestLat() {
		return southWestLat;
	}
	public void setSouthWestLat(double southWestLat) {
		this.southWestLat = southWestLat;
	}
	public double getSouthWestLng() {
		return southWestLng;
	}
	public void setSouthWestLng(double southWestLng) {
		this.southWestLng = southWestLng;
	}
	public String getLocationLat() {
		return locationLat;
	}
	public void setLocationLat(String locationLat) {
		this.locationLat = locationLat;
	}
	public String getLocationLng() {
		return locationLng;
	}
	public void setLocationLng(String locationLng) {
		this.locationLng = locationLng;
	}
	public String getBedrooms() {
		return bedrooms;
	}
	public void setBedrooms(String bedrooms) {
		this.bedrooms = bedrooms;
	}
	public String getBathrooms() {
		return bathrooms;
	}
	public void setBathrooms(String bathrooms) {
		this.bathrooms = bathrooms;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public String getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}
	public double getNorthEastLat() {
		return northEastLat;
	}
	public boolean isRantangleSearch() {
		return rantangleSearch;
	}
	public void setRantangleSearch(boolean rantangleSearch) {
		this.rantangleSearch = rantangleSearch;
	}
	public boolean isPolygonSearch() {
		return polygonSearch;
	}
	public void setPolygonSearch(boolean polygonSearch) {
		this.polygonSearch = polygonSearch;
	}
	public double getCircleCenterLat() {
		return circleCenterLat;
	}
	public void setCircleCenterLat(double circleCenterLat) {
		this.circleCenterLat = circleCenterLat;
	}
	public double getCircleCenterLng() {
		return circleCenterLng;
	}
	public void setCircleCenterLng(double circleCenterLng) {
		this.circleCenterLng = circleCenterLng;
	}
	public double getCircleRadius() {
		return circleRadius;
	}
	public void setCircleRadius(double circleRadius) {
		this.circleRadius = circleRadius;
	}
	public boolean isCircleSearch() {
		return circleSearch;
	}
	public void setCircleSearch(boolean circleSearch) {
		this.circleSearch = circleSearch;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

}
