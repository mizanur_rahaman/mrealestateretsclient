package com.realestate.client.dao;

import java.util.Iterator;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class TestMongo {
   public static void main(String args[])
   {
	   MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
	      MongoCredential credential; 
		credential = MongoCredential.createCredential("mongo", "mongodb", 
		  "password".toCharArray()); 
		System.out.println("Connected to the database successfully");  
		
		// Accessing the database 
		MongoDatabase database = mongo.getDatabase("north_star_mls_db");
		 MongoCollection<Document> collection = database.getCollection("single_family"); 
		 BasicDBObject criteria = new BasicDBObject();
		 criteria.put("AgentOwner", 0);
	
		 FindIterable<Document> iterDoc = collection.find(criteria).limit(5); 
	     int i = 1; 

	      // Getting the iterator 
	     Iterator it = iterDoc.iterator(); 
	    
	      while (it.hasNext()) {  
	         System.out.println(it.next());  
	      i++; 
	      }
   }
}
