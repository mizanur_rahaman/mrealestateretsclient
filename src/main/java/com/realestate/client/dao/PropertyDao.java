package com.realestate.client.dao;

import java.util.ArrayList;
import java.util.Iterator;
import org.bson.Document;
import org.springframework.stereotype.Repository;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.realestate.client.dto.ResultDto;
@Repository
public class PropertyDao {
	/*@Autowired MongoTemplate mongoTemplate;
	public void getSingleFamilyHouses(MongoDatabase database)
	{
		 System.out.println("single family houses");
		 List<SingleFamilyDto> singleFamilys = mongoTemplate.find(Query.query(Criteria.where("Bedrooms").is(3)),SingleFamilyDto.class);
		 System.out.println(singleFamilys.size());
	}*/
	
	public ResultDto getSingleFamilyHouses(MongoDatabase database, Document criteria, int offset, int limit)
	{
		 System.out.println("single family houses");
		 ResultDto resultDto = new ResultDto();
		 try 
		 {
			 MongoCollection<Document> collection = database.getCollection("Single-Family"); 
				
		     long count = collection.count(criteria);
			 FindIterable<Document> iterDoc = collection.find(criteria).skip(offset).limit(limit); 
		  
		      // Getting the iterator 
		     Iterator it = iterDoc.iterator(); 
		     ArrayList<Document> dataList = new ArrayList<Document>();
		     while (it.hasNext()) {  
		         Document doc = (Document) it.next();
		         dataList.add(doc);
		      }
		     
		     resultDto.setTotal(count);
		     resultDto.setData(dataList);
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
		 }
		 finally {
			 
		 }
		 
	     return resultDto;

	}
	
	public void updateDocumentSingleFamilyHouse( MongoDatabase database, Document searchQuery, Document newDocument)
	{
		MongoCollection<Document> collection = database.getCollection("Single-Family"); 
		collection.updateOne(searchQuery, newDocument);
	}
	
	
}
