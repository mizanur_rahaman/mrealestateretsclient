package com.realestate.client.dao;

import java.util.Iterator;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.realestate.client.manager.connection.MongoDbConnectionManager;

@Repository
public class RealEstateDataDao {

	@Autowired MongoDbConnectionManager mongoDbConnectionManager;
	
	public void insertNewProperty(String properyName, String className)
	{
		 MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		 MongoCollection<Document> collection = database.getCollection("property"); 
	     System.out.println("Collection sampleCollection selected successfully " +properyName+" dls");
	     
	      Document document = new Document("Property Name", properyName) 
	      .append("class Name", className);  
	      collection.insertOne(document);
	}
	
	public void insertNewHouse(Document document, String tableName)
	{
		MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		MongoCollection<Document> collection = database.getCollection(tableName); 
		collection.insertOne(document);
	}
	
	public void addSingleFamilyHousesImage( Document searchQuery, Document newDocument)
	{
		MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		MongoCollection<Document> collection = database.getCollection("single_family"); 
		collection.updateOne(searchQuery, newDocument);
	}
}
