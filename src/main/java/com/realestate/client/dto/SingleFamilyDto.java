package com.realestate.client.dto;

public class SingleFamilyDto {
	private int bedRooms;
	private int bathRooms;
	public int getBedRooms() {
		return bedRooms;
	}
	public void setBedRooms(int bedRooms) {
		this.bedRooms = bedRooms;
	}
	public int getBathRooms() {
		return bathRooms;
	}
	public void setBathRooms(int bathRooms) {
		this.bathRooms = bathRooms;
	}
}
