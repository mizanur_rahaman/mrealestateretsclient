package com.realestate.client.dto;

import java.util.ArrayList;

import org.bson.Document;

public class ResultDto {

    long total;
	ArrayList<Document> data = new ArrayList<Document>();
	public long getTotal() {
		return total;
	}
	public void setTotal(long count) {
		this.total = count;
	}
	public ArrayList<Document> getData() {
		return data;
	}
	public void setData(ArrayList<Document> data) {
		this.data = data;
	}
}
